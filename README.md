# Tangelo technical test


## Installation
create and activate an enviroment (on linux)
```bash
virtualenv venv
source venv/bin/activate
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.

```bash
pip install -r requirements.txt
```

## Usage

```bash
python3 test.py
```

## Contributing
Feel free to test or change the methods of the ```test.py``` file.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
