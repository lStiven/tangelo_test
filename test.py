import pandas as pd
import requests
import hashlib
import time
import sqlite3

URL = "https://restcountries.com/v3.1/all"


def get_country_data() -> pd.DataFrame:
    """
    Create a dataframe with info about country API.

    ...

    Parameters
    ----------

    Raises
    ------


    Returns
    -------
    pandas.DataFrame

    """
    DF_COLUMNS: tuple = ("Region", "City Name", "Language", "Time (ms)")
    response = requests.get(URL).json()
    data: list = []

    for item in response:
        row_start_time: float = time.time()
        data.append(
            [
                item.get("region"),
                item.get("name").get("common"),
                hashlib.sha1(
                    list(item.get("languages").values())[0].encode()
                ).hexdigest()
                if item.get("languages")
                else "",
                (time.time() - row_start_time) * 1000,
            ]
        )
    df = pd.DataFrame(data, columns=DF_COLUMNS)
    dataframe_to_json(df)
    dataframe_to_sqlite(df)
    print(dataframe_stats(df, ["Time (ms)"]))
    return df


def dataframe_to_json(
    df: pd.DataFrame,
    path: str = "./",
    file_name: str = "data",
    orient: str = "records",
):

    """
    Exports pandas DataFrame to JSON file.

    ...

    Parameters
    ----------
    df : pandas.DataFrame
        Data Frame to export

    path : str, default: current path
        path to export file

    file_name : str, default: data
        file name


    orient : split | records | index | values | table | columns, default: records
        format the data according to the orientation

    Raises
    ------

    Returns
    -------
    None

    """
    df.to_json(f"{path}{file_name}.json", orient=orient, indent=4)


def dataframe_to_sqlite(
    df: pd.DataFrame, path: str = "./databse.sqlite", database_name: str = "test"
):
    """export pandas DataFrame to sqlite

    Args:
        df (pandas.DataFrame): dataframe
        path (str, optional): path to sqlite file. Defaults to "./databse.sqlite".
        database_name (str, optional): Database name. Defaults to "test".
    """
    conn = sqlite3.connect(path)
    df.to_sql(name=database_name, con=conn, if_exists="replace", index=True)
    conn.close()


def dataframe_stats(df: pd.DataFrame, columns: list) -> pd.DataFrame:
    """returns description of the data in the DataFrame

    Args:
        df (pd.DataFrame): dataframe
        columns (list): list of columns to extract info

    Returns:
        pandas.DataFrame: describe dataframe
    """
    describe = df[columns].describe()
    describe.loc["total"] = df[columns].sum()
    return describe


get_country_data()
